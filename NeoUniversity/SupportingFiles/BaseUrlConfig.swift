//
//  BaseUrlConfig.swift
//  NeoStudent
//
//  Created by Kota Naveen Kumar on 10/06/21.
//

import Foundation

struct NeoUniversityConstants {
    static let UserType = "SUB_ADMIN"//"SUB_ADMIN"
    static let accessToken = UserDefaults.standard.value(forKey:"ACCESSTOKEN") //
    static let refeshToken = UserDefaults.standard.value(forKey:"REFRESHTOKEN")
    static let universityId = UserDefaults.standard.value(forKey:"UNIVERSITYID")


}



struct signInViewControllerConstants {
    static let email_otp = "EMAIL_OTP"
    static let mobile_Otp = "MOBILE_OTP"
    static let userName_Password = "USERNAME_PASSWORD"
}


//MARK:- OTPValidationType
// BOTH, EMAIL, MOBILE, SINGLE_SIGNUP_OTP


struct otpViewControllerConstants{
    static let both = "BOTH"
    static let email = "EMAIL"
    static let mobile = "MOBILE"
    static let single_signup_otp = "SINGLE_SIGNUP_OTP"
}


struct BaseAPI {
    
    #if Prod
    static let baseURLString = "http://3.108.127.233:8080/neomediaapis/"
    #elseif Dev
    static let baseURLString = "http://3.108.127.233:8080/neomediaapis/"
    //"http://15.206.81.74:8080/neomediaapis/"
    #elseif Stage
    static let baseURLString = "http://3.108.127.233:8080/neomediaapis/"
    #endif
    
}


protocol UrlConverting {
    func url() -> URL
}

enum NeoUniversityRequestUrl {
    // login Module
    case SignIn
    
    // otpValidate
    case OtpValide
    
    // get university Students
    case universityGet
}
extension NeoUniversityRequestUrl: UrlConverting {
    func url() -> URL {
        
        switch self {
        case .SignIn:
                    let SignIn = "auth/signin"
                    return URL(string: "\(BaseAPI.baseURLString)\(SignIn)")!
            
        case .OtpValide:
            let SendOTP = "otp/validate"
            return URL(string: "\(BaseAPI.baseURLString)\(SendOTP)")!
            
        case .universityGet:
            let SendOTP = "university/get?id=UINIVERSITYID"
            return URL(string: "\(BaseAPI.baseURLString)\(SendOTP)")!
            
        }
        
    }
    
}

//
//  UIButtonExtentions.swift
//  Tropogo
//
//  Created by  Naveen Kumar on 14/04/21.
//

import Foundation
import UIKit

enum buttonStyles {
    case PrimaryNormal
    //case PrimaryMedium
    case Secondary
    
}


extension UIButton {
    
    //Primary Button  Active Style
    func ButtonStylePrimaryActive(title:String){
        layer.cornerRadius = 5
        layer.masksToBounds = true
        let color = UIColor.title
        self.clipsToBounds = true
        self.setColors(color: color, tintColor:UIColor.white, Style: .PrimaryNormal, title: title)
        
    }
    
    //Primary Button  InActive Style
    func ButtonStylePrimaryInActive(title:String){
        self.clipsToBounds = true
        layer.cornerRadius = 5
        layer.masksToBounds = true
        let color = UIColor(red: 0.039, green: 0.478, blue: 1, alpha: 0.4)
        self.setColors(color: color, tintColor: .white, Style: .Secondary, title: title)
        
    }
    
   
    //Secondry Button Active Style
//    func ButtonStyleSecondryActive(title:String){
//        layer.cornerRadius = 6
//        self.clipsToBounds = true
//        layer.masksToBounds = true
//        backgroundColor = UIColor.white
//        layer.borderWidth = 1
//        layer.borderColor = UIColor.colorTag_Blue.cgColor
//        self.setColors(color: .white, tintColor: .colorTag_Blue, Style: .Secondary, title: title)
//
//    }
//
//    //Secondry Button InActive Style
//
//    func ButtonStyleSecondryInActive(title:String){
//        layer.cornerRadius = 6
//        self.clipsToBounds = true
//        layer.masksToBounds = true
//        backgroundColor = UIColor.white
//        layer.borderWidth = 1
//        layer.borderColor = UIColor(red: 0.039, green: 0.478, blue: 1, alpha: 0.4).cgColor
//        let color = UIColor(red: 0.039, green: 0.478, blue: 1, alpha: 0.4)
//        self.setColors(color: .white, tintColor:color , Style: .Secondary, title: title)
//
//    }
    
    
    func setColors(color:UIColor,tintColor:UIColor,Style:buttonStyles,title:String){
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.setBackgroundImage(colorImage, for: .normal)
           // self.titleLabel?.tintColor = tintColor
            self.setTitleColor(tintColor, for: .normal)
            //self.titleLabel?.textColor = tintColor
            self.setTitle(title, for: .normal)
//            if Style == .PrimaryNormal {
//                self.titleLabel?.font = UIFont().SFProTextFontStyle(style: SFProTextFont.SFProTextSemiBold, size: 12)
//            }else{
//                self.titleLabel?.font = UIFont().SFProTextFontStyle(style: SFProTextFont.SFProTextRegular, size: 15)
//            }
          
        }
    }
}

extension String {
   var isValidEmail: Bool {
      let regularExpressionForEmail = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
      let testEmail = NSPredicate(format:"SELF MATCHES %@", regularExpressionForEmail)
      return testEmail.evaluate(with: self)
   }
   var isValidPhone: Bool {
      let regularExpressionForPhone = "^\\d{3}-\\d{3}-\\d{4}$"
      let testPhone = NSPredicate(format:"SELF MATCHES %@", regularExpressionForPhone)
      return testPhone.evaluate(with: self)
   }
}



//extension UIButton {
//
//    func applayInActiveTheam() {
//
//        layer.cornerRadius = 8
//        layer.borderWidth = 1
//        layer.borderColor = UIColor.secondryColor.cgColor
//       // titleLabel?.tintColor = .secondryColor
//        self.titleLabel?.textColor = .secondryColor
//        self.setTitleColor(.secondryColor, for: .normal)
//
//        titleLabel?.font = UIFont().SFProTextFontStyle(style: SFProTextFont.SFProTextRegular, size: 13)
//
//    }
//
//
//    func applayActiveTheam() {
//        layer.cornerRadius = 8
//        layer.borderWidth = 1
//        layer.borderColor = UIColor.colorTag_Blue.cgColor
//      //  titleLabel?.tintColor = .colorTag_Blue
//        titleLabel?.textColor = .colorTag_Blue
//        self.setTitleColor(.colorTag_Blue, for: .normal)
//        titleLabel?.font = UIFont().SFProTextFontStyle(style: SFProTextFont.SFProTextRegular, size: 13)
//
//    }
//
//    func applayGenderActiveTheam() {
//        layer.cornerRadius = 8
//        layer.borderWidth = 1.5
//        layer.borderColor = UIColor.colorTag_Blue.cgColor
//     //   titleLabel?.tintColor = .colorTag_Blue
//        titleLabel?.textColor = .colorTag_Blue
//        self.setTitleColor(.colorTag_Blue, for: .normal)
//        titleLabel?.font = UIFont().SFProTextFontStyle(style: SFProTextFont.SFProTextRegular, size: 13)
//
//    }
//
//}

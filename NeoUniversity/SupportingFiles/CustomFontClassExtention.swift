//
//  CustomFontClassExtention.swift
//  Tropogo
//
//  Created by  Naveen Kumar on 09/04/21.
//

import Foundation
import UIKit



enum SFUITextFont: String {
    
    case SFUITextRegular     = "SFUIText-Regular"
    case SFUITextSemibold    = "SFUIText-Semibold"
    case SFUITextBold        = "SFUIText-Bold"
    case SFUITextMedium      = "SFUIText-Medium"
    
}

extension UIFont {
    
    func SFUITextFontStyle(style:SFUITextFont ,size:CGFloat) -> UIFont {
        return UIFont(name:style.rawValue, size:size)!
    }
    
}



extension UIView {
    
    func UIViewStyle(){
        // let button = UIButton()
        layer.cornerRadius = 8
        layer.masksToBounds = true
        layer.borderColor = UIColor.lightGray.cgColor
        layer.borderWidth = 0
        
    }
}

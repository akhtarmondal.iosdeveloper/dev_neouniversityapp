//
//  HomeUniversityViewModel.swift
//  NeoUniversity
//
//  Created by Kota Naveen Kumar on 16/07/21.
//

import Foundation


class HomeUniversityViewModel {
    
    fileprivate let networkClient: NetworkClientProtocol
    
    init(networkClient: NetworkClientProtocol = NetworkClient.sharedInstance) {
        self.networkClient = networkClient
    }
    
}

extension HomeUniversityViewModel {
    
    func getUniversityStudentsApi(method:MethodType,bodyparameters:getUniversityStudentRequestModel ,onComplete: @escaping(GetUniversityStudentResponseFinalModel) -> ()){
        
        let url = NeoUniversityRequestUrl.universityGet.url()
        let myapi = url.absoluteString
      //  myapi = myapi.replacingCharacters(in:"UINIVERSITYID", with:)
        let updatedApi = myapi.replacingOccurrences(of:"UINIVERSITYID", with: bodyparameters.id!)
        let request = URLRequest.jsonRequest(url: URL(string: updatedApi)!)
        self.networkClient.sendRequestGeneric(request: request, methodType:.get, bodyParams: bodyparameters){ (data, resp, err) in
            if let httpResponse = resp as? HTTPURLResponse {
                if httpResponse.statusCode != 200 && httpResponse.statusCode != 201 {
                    do {
                        
                        let res = try JSONSerialization.jsonObject(with: data!) as? [String: Any]
                        debugPrint(res)
                        let final = GetUniversityStudentResponseFinalModel.init(status: false, response: nil)
                        onComplete(final)
                        
                       
                        
                    } catch let parsingError {
                        debugPrint("Error", parsingError)
                    }
                } else {
                    
                    do {
                        let Response = try JSONDecoder().decode(GetUniversityStudentResponseModel.self, from: data!)
                        let bb = GetUniversityStudentResponseFinalModel.init(status: true, response: Response)
                        onComplete(bb)
                        
                    } catch let parsingError {
                        debugPrint("Error", parsingError)
                    }
                    
                }
                
                
            }
            
        }
    }
    
}

//
//  HomeUniversityModel.swift
//  NeoUniversity
//
//  Created by Kota Naveen Kumar on 16/07/21.
//

import Foundation


struct getUniversityStudentRequestModel:Codable {
    let id:String?
    
}

struct GetUniversityStudentResponseFinalModel {
    let status:Bool
    let response:GetUniversityStudentResponseModel?
}



// MARK: - Enrollment
struct Enrollment: Codable {
    let status: String?
    let university: GetUniversityStudentResponseModel?
    let course: Course?
}

// MARK: - Student
struct Student: Codable {
    let id, createdAt, updatedAt, email: String?
    let mobile: Mobile?
    let firstname, lastname: String?
    let enrollment: Enrollment?
}

// MARK: - GetUniversityStudentResponseModel
struct GetUniversityStudentResponseModel: Codable {
    let id, name, getUniversityStudentResponseModelDescription, logoURL: String?
    let bannerImageURL: String?
    let establishmentYear: Int?
    let scholorshipAvailable: Bool?
    let intakePeriod, staff: Int?
    let accommodationAvailable: Bool?
    let email, websiteURL, address: String?
    let mobile: Mobile?
   // let createBy, updatedBy: JSONNull?
    let createdAt, updatedAt: String?
    let course: [Course]?
    //let faqs: [FAQ]?
    let student: [Student]?

    enum CodingKeys: String, CodingKey {
        case id, name
        case getUniversityStudentResponseModelDescription = "description"
        case logoURL = "logoUrl"
        case bannerImageURL = "bannerImageUrl"
        case establishmentYear, scholorshipAvailable, intakePeriod, staff, accommodationAvailable, email
        case websiteURL = "websiteUrl"
        case address, mobile, updatedAt, createdAt,course, student
       // case updatedBy,createBy ,faqs
    }
}

// MARK: - Course
struct Course: Codable {
    let id, name, courseDescription: String?
    let courseDuration: Int?
    let inTakePeriod, eligibilityCriteria, courseType, coursePeriod: String?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case courseDescription = "description"
        case courseDuration, inTakePeriod, eligibilityCriteria, courseType, coursePeriod, createdAt, updatedAt
    }
}

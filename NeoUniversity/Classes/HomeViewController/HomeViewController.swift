//
//  HomeViewController.swift
//  NeoUniversity
//
//  Created by naveen kumar on 30/05/21.
//

import UIKit
import JWTDecode

class HomeViewController: UIViewController,UISearchBarDelegate {
    
    
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var searchlabel: UILabel!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var broadCastButton: UIButton!
    
    @IBOutlet weak var exportButton: UIButton!
    
    @IBOutlet weak var selectAllButton: UIButton!
    
    @IBOutlet weak var selectAllDescriptionLabel: UILabel!
    
    @IBOutlet weak var tabelView: UITableView!
    
    @IBOutlet weak var addButton: UIButton!
    
    let viewModel = HomeUniversityViewModel ()
    var response:GetUniversityStudentResponseModel?
    
    
    
    
    var searchedStudent = [String]()
      
       var searching = false
    var count = 0
    
    let studentArr = ["raam", "thulasi", "prasad", "venkatesh", "suresh", "ramesh", "naveen", "krishna","mohan","kumar","ram","prasad"]
       

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        //searchlabel.text = "Search"
//searchView.backgroundColor = .lightGray
        self.searchView.UIViewStyle()
        searchbar.delegate = self
        selectAllDescriptionLabel.text = "Selected  4 of 37 entries"
        
        if response != nil {
            tabelView.delegate = self
            tabelView.dataSource = self
            tabelView.tableFooterView = UIView()
            tabelView.reloadData()
        }else{
            
        }

    }
    
  
    
    //MARK:- Button Actions
    
    @IBAction func notificationButtonAction(_ sender: Any) {
        
        let nvc = (storyboard?.instantiateViewController(withIdentifier: "NotificationViewController"))! 
        self.navigationController?.pushViewController(nvc, animated: true)    }
    
    @IBAction func filterAct(_ sender: UIButton) {
        
        let fvc = (storyboard?.instantiateViewController(withIdentifier: "FilterViewController"))!
        fvc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(fvc, animated: true, completion: nil)
       
    }
    
    @IBAction func sortAct(_ sender: UIButton) {
        let svc = (storyboard?.instantiateViewController(withIdentifier: "SortViewController"))!
        self.navigationController?.present(svc, animated: true, completion: nil)
        
    }
    
    @IBAction func BroadCastAct(_ sender: UIButton) {
        
        let savc = (storyboard?.instantiateViewController(withIdentifier: "AlertViewController"))!
        self.navigationController?.present(savc, animated: true, completion: nil)
    }
    @IBAction func exportAct(_ sender: UIButton) {
        
    }
    
    @IBAction func selectAllAct(_ sender: UIButton) {
        
        
    }
    
    @IBAction func addButtonAct(_ sender: UIButton) {
        let asvc = (storyboard?.instantiateViewController(withIdentifier: "AddStudentViewController"))!
        self.navigationController?.present(asvc, animated: true, completion: nil)

    }
    @IBAction func nextViewButton(_ sender: Any) {
        let pvc = (storyboard?.instantiateViewController(withIdentifier: "PersonalViewController"))!
        self.navigationController?.pushViewController(pvc, animated: true)
    }
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
        {
            if searchbar.text == nil || searchbar.text == "" {
                searching = false
                // to dismiss keyboard
                view.endEditing(true)
                tabelView.reloadData()
            }else{
                searching = true
            searchedStudent = studentArr.filter { $0.range(of: searchText, options: .caseInsensitive) != nil }
                tabelView.reloadData()
            }
        }
        
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
        {
            searching = false
            searchbar.text = ""
            tabelView.reloadData()
        }
}



extension HomeViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if searching {
            //selectAllDescriptionLabel.text = "Selected  \(count) of \(searchedStudent.count) entries"
                    return (response?.student!.count)!
            
            
                } else {
                    //selectAllDescriptionLabel.text = "Selected  \(count) of \(studentArr.count) entries"
                    
                    
                    return (response?.student!.count)!
                }
       // return 10
    }
    
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        
        let cell = tabelView.dequeueReusableCell(withIdentifier:"HomeTableViewCell") as! HomeTableViewCell
        cell.selectCellBoxButton.setImage(UIImage(named: "UnselectedBox"), for: .normal)
        
           //  assign the index of the cell  to button tag
              cell.selectCellBoxButton.tag = indexPath.row
              
              // call the subscribeTapped method when tapped
              cell.selectCellBoxButton.addTarget(self, action: #selector(selectedBox(_:)), for: .touchUpInside)
 //       cell.nameLabel.text = studentArr[indexPath.row]
        
        if searching
        {
            cell.nameLabel.text = searchedStudent[indexPath.row]
        }else{
            cell.nameLabel.text = "\(response?.student?[indexPath.row].firstname ?? "") \(response?.student?[indexPath.row].lastname ?? "")"
            cell.emailLabel.text = response?.student?[indexPath.row].email
            cell.ArrdateLabel.text = response?.student?[indexPath.row].createdAt
        }
        
        return cell
    }
    
    @objc func selectedBox(_ sender: UIButton){
      // use the tag of button as index
        
        
        if sender.image(for: UIControl.State.normal) == UIImage(named: "UnselectedBox") {
            sender.setImage(UIImage(named: "selectedBox"), for: UIControl.State.normal)
            count = count + 1
            print ("selected count  is \(count)")
            
            selectAllDescriptionLabel.text = "Selected  \(count) of \(studentArr.count) entries"
                    
            }else {
                sender.setImage(UIImage(named: "UnselectedBox"), for: UIControl.State.normal)
                count = count - 1
                selectAllDescriptionLabel.text = "Selected  \(count) of \(studentArr.count) entries"
                print ("selected count  is \(count)")
            }
       
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           tableView.deselectRow(at: indexPath, animated: true)
           
           if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            print(indexPath.row)
            //               if cell.accessoryType == .checkmark{
//                   cell.accessoryType = .none
//
//               }
//               else{
//                   cell.accessoryType = .checkmark
//               }
            
           
            
           }
           
       }
    
   
    
}

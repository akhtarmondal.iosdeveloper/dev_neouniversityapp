//
//  ViewController.swift
//  NeoUniversity
//
//  Created by naveen kumar on 25/05/21.
//

import UIKit
import SKActivityIndicatorView

class ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var email_TextField: UITextField!
    @IBOutlet weak var university_NameTextField: UITextField!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    let viewModel = LoginViewModel()
    var response : FinalSignInModel?
    
    
    var emailid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.isHidden = true
        email_TextField.delegate = self
        errorLabel.isHidden = true
        
        email_TextField.text = "bangUnivSubAdmin@gmail.com"
        university_NameTextField.text = "sss"
       
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
    
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if (email_TextField.text?.isValidEmail)!
            {
            email_TextField.setupRightImage(imageName: "Check_Mark")
          //  errorLabel.isHidden = true
            }
        else
            {
                email_TextField.setupRightImage(imageName: "")
                   // print("print error label")
               // errorLabel.isHidden = false
            }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (email_TextField.text?.isValidEmail)!
            {
            email_TextField.setupRightImage(imageName: "Check_Mark")
        errorLabel.isHidden = true
            }
        else
            {
                email_TextField.setupRightImage(imageName: "")
                  print("print error label")
        errorLabel.isHidden = false
           }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        
    }

    
    func validate()-> Bool {
            
            let thereAreEmptyTextFields = [email_TextField, university_NameTextField].map { $0.text ?? "" }.contains { $0.isEmpty }
            
            return thereAreEmptyTextFields
        }
    
    
   //MARK:- SignUP Api
        
        
        func signInApiCall() {
            
          //  let mobileBody = Mobile.init(countryCode: 91, number: 9844116372)
            
            let signInType = signInViewControllerConstants.email_otp
            let body = SignInRequestModel.init(email:email_TextField.text, mobileNumber: nil, signinWith: signInType, userType: NeoUniversityConstants.UserType)
            
            viewModel.signInApiCall(method: .POST, bodyparameters:body){
                [weak self] (resp) in
                
                guard self != nil else {return}
                
                self?.signInAPiDisplay(resp: resp)
                
                
            }
        }
        
        
        func signInAPiDisplay(resp:FinalSignInModel) {
            
            SKActivityIndicator.dismiss()
            
            
            if resp.errorResponse == nil && resp.sucessResponce?.otpSent == true {
                
                debugPrint(resp)
                
                let vc = UIStoryboard(name: "Main", bundle: nil)
                let sb = vc.instantiateViewController(withIdentifier: "OTPVerificationViewController") as! OTPVerificationViewController
                sb.signInInputModel = resp.sucessResponce
                self.navigationController?.pushViewController(sb, animated: true)
                
                
                
                
            }else{
                UtilityClass.displayAlertWith((resp.errorResponse?.message)!)
            }
            
        }
    
    @IBAction func NextButtonAction(_ sender: Any) {
        
        signInApiCall()
        
        
//        let value = self.validate()
//
//                if value == true {
//
//                    let alert = UIAlertController(title: "Alert", message: "please enter all details", preferredStyle: .alert)
//
//                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//
//                    self.present(alert, animated: true)
//
//                }else{
//
//                }

    }
        
}
extension UITextField {

    //MARK:- Set Image on the right of text fields

  func setupRightImage(imageName:String){
    let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
    imageView.image = UIImage(named: imageName)
    let imageContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 55, height: 40))
    imageContainerView.addSubview(imageView)
    rightView = imageContainerView
    rightViewMode = .always
    self.tintColor = .lightGray
}

 //MARK:- Set Image on left of text fields

    func setupLeftImage(imageName:String){
       let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
       imageView.image = UIImage(named: imageName)
       let imageContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 55, height: 40))
       imageContainerView.addSubview(imageView)
       leftView = imageContainerView
       leftViewMode = .always
       self.tintColor = .lightGray
     }

  }




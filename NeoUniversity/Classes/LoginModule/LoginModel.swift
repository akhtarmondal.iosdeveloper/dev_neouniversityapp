//
//  LoginModel.swift
//  NeoStudent
//
//  Created by Kota Naveen Kumar on 10/06/21.
//

import Foundation

// MARK: - SignUpRequestModel
struct SignUpRequestModel: Codable {
    let email: String?
    let mobile: Mobile?
    let userType: String?
}

// MARK: - Mobile
struct Mobile: Codable {
    let countryCode, number: Int?
}



// MARK: - SignUpResponseModel
struct SignUpResponseModel: Codable {
    let userType: String?
    let emailOtp: EmailOtp?
    let mobileOtp: MobileOtp?
    let otpSent: Bool?
}

// MARK: - EmailOtp
struct EmailOtp: Codable {
    let email: String?
    let otp: Int?
}

// MARK: - MobileOtp
struct MobileOtp: Codable {
    let mobile: Mobile?
    let otp: Int?
}





// MARK: - SignInRequestModel
struct SignInRequestModel: Codable {
    let email: String?
    let mobileNumber: Mobile?
    let signinWith, userType: String?
}

// MARK: - SignInResponseModel

struct SignInResponseModel: Codable {
    let userType, signinWith: String?
       let otpSent: Bool?
       let mobileOtp: MobileOtp?
       let emailOtp: EmailOtp?
      // let authToken: JSONNull? // no need
}


// MARK: - ErrorModel
struct ErrorModel: Codable {
    let error: Bool?
    let status, type, message: String?
}
 

// MARK: - FinalSignInModel

struct FinalSignInModel {
    let sucessResponce: SignInResponseModel?
    let errorResponse : ErrorModel?
}

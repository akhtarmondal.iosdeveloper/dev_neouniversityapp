//
//  LoginViewModel.swift
//  NeoStudent
//
//  Created by Kota Naveen Kumar on 10/06/21.
//

import Foundation


class LoginViewModel {
    
    fileprivate let networkClient: NetworkClientProtocol
    
    init(networkClient: NetworkClientProtocol = NetworkClient.sharedInstance) {
        self.networkClient = networkClient
    }
    
}

extension LoginViewModel {
    
    func signInApiCall(method:MethodType,bodyparameters:SignInRequestModel ,onComplete: @escaping(FinalSignInModel) -> ()){
        
        let url = NeoUniversityRequestUrl.SignIn.url()
        
        let request = URLRequest.jsonRequest(url: url)
        self.networkClient.sendRequestGeneric(request: request, methodType: .post, bodyParams: bodyparameters){ (data, resp, err) in
            if let httpResponse = resp as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    
                    do {
                        let Response = try JSONDecoder().decode(ErrorModel.self, from: data!)
                        
                        let final = FinalSignInModel.init(sucessResponce:nil, errorResponse: Response)
                        onComplete(final)
                        
                    } catch let parsingError {
                        debugPrint("Error", parsingError)
                    }
                   
                } else {
                    
                    do {
                        let Response = try JSONDecoder().decode(SignInResponseModel.self, from: data!)
                        
                        let final = FinalSignInModel.init(sucessResponce:Response, errorResponse: nil)
                        onComplete(final)
                        
                    } catch let parsingError {
                        debugPrint("Error", parsingError)
                    }
                    
                }
                
                
            }
            
        }
    }
    
}

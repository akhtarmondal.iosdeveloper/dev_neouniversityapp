//
//  MainTabBar.swift
//  CustomTabBar
//
//  Created by  Naveen Kumar on 21/05/21.
//

import UIKit
import Foundation

class MainTabBar: UITabBar {
    
    var color: UIColor?
    var radii: Float = 15.0
    private var shapeLayer: CALayer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.masksToBounds = true
        layer.cornerRadius = 30
        UITabBar.appearance().unselectedItemTintColor = UIColor.title
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 0.3
     
        self.backgroundColor = .white
        self.frame = CGRect(x: 25, y: 0, width: self.frame.width - 50, height: self.frame.height - 100)
        self.layoutIfNeeded()

    }
    
    override func draw(_ rect: CGRect) {
      //  self.addShape()
    }
    
    
   
     
        private func addShape() {
            let shapeLayer = CAShapeLayer()
            shapeLayer.path = createPath()
            shapeLayer.strokeColor = UIColor.lightGray.cgColor
            shapeLayer.fillColor = UIColor.white.cgColor
            shapeLayer.lineWidth = 1.0

            //The below 4 lines are for shadow above the bar. you can skip them if you do not want a shadow
            shapeLayer.shadowOffset = CGSize(width:0, height:0)
            shapeLayer.shadowRadius = 10
            shapeLayer.shadowColor = UIColor.gray.cgColor
            shapeLayer.shadowOpacity = 0.3

            if let oldShapeLayer = self.shapeLayer {
                self.layer.replaceSublayer(oldShapeLayer, with: shapeLayer)
            } else {
                self.layer.insertSublayer(shapeLayer, at: 0)
            }
            self.shapeLayer = shapeLayer
        }

        func createPath() -> CGPath {
            let height: CGFloat = 37.0
            let path = UIBezierPath()
            let centerWidth = self.frame.width
            path.move(to: CGPoint(x: 0, y: 60)) // start top left
            path.addLine(to: CGPoint(x: (centerWidth - height * 2), y: 60)) // the beginning of the trough

            path.addCurve(to: CGPoint(x: centerWidth, y: height),
            controlPoint1: CGPoint(x: (centerWidth - 30), y: 60), controlPoint2: CGPoint(x: centerWidth - 35, y: height))

            path.addCurve(to: CGPoint(x: (centerWidth + height * 2), y: 0),
            controlPoint1: CGPoint(x: centerWidth + 35, y: height), controlPoint2: CGPoint(x: (centerWidth + 30), y: 0))

            path.addLine(to: CGPoint(x: self.frame.width, y: 0))
            path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height))
            path.addLine(to: CGPoint(x: 0, y: self.frame.height))
            path.close()

            return path.cgPath
           }

}

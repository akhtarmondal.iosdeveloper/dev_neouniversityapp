//
//  FilterViewController.swift
//  NeoUniversity
//
//  Created by naveen kumar on 28/05/21.
//

import UIKit
import iOSDropDown

class FilterViewController: UIViewController,UITextFieldDelegate {


    @IBOutlet weak var ApplyButton: UIButton!

   
    @IBOutlet weak var countryOrigion: DropDown!
    
    @IBOutlet weak var countryDeparture: DropDown!
    
    @IBOutlet weak var cityDeparture: DropDown!
    
    @IBOutlet weak var countryArrival: DropDown!
    
    @IBOutlet weak var cityOrigion: DropDown!
    @IBOutlet weak var course: DropDown!
    
    @IBOutlet weak var gender: DropDown!
    @IBOutlet weak var intake: DropDown!
    override func viewDidLoad() {
            super.viewDidLoad()
        
            self.ApplyButton.ButtonStylePrimaryActive(title: "APPLY FILTERS")
        //country of region
            let NamesArry:[String] = ["Yes","No"]
            countryOrigion.delegate = self
            countryOrigion.optionArray = NamesArry
            countryOrigion.selectedRowColor = .white
            countryOrigion.listHeight = 300
            countryOrigion.rowHeight = 60
        countryOrigion.arrowColor  = UIColor.orange
          //  countryOrigion.arrowSize = 10
            countryOrigion.didSelect{(selectedText , index ,id) in
            self.countryOrigion.text = selectedText
                }
        //country Departure
          //  let NamesArry:[String] = ["Yes","No"]
        countryDeparture.delegate = self
        countryDeparture.optionArray = NamesArry
        countryDeparture.selectedRowColor = .white
        countryDeparture.listHeight = 300
        countryDeparture.rowHeight = 60
        countryDeparture.arrowColor  = UIColor.orange
          //  countryOrigion.arrowSize = 10
        countryDeparture.didSelect{(selectedText , index ,id) in

                self.countryDeparture.text = selectedText
                }
        
        
        //cityDeparture
          //  let NamesArry:[String] = ["Yes","No"]
        cityDeparture.delegate = self
        cityDeparture.optionArray = NamesArry
        cityDeparture.selectedRowColor = .white
        cityDeparture.listHeight = 300
        cityDeparture.rowHeight = 60
        cityDeparture.arrowColor  = UIColor.orange
          //  countryOrigion.arrowSize = 10
        cityDeparture.didSelect{(selectedText , index ,id) in

                self.cityDeparture.text = selectedText
                }
        
        //countryArrival
          //  let NamesArry:[String] = ["Yes","No"]
        countryArrival.delegate = self
        countryArrival.optionArray = NamesArry
        countryArrival.selectedRowColor = .white
        countryArrival.listHeight = 300
        countryArrival.rowHeight = 60
        countryArrival.arrowColor  = UIColor.orange
          //  countryOrigion.arrowSize = 10
        countryArrival.didSelect{(selectedText , index ,id) in

                self.countryArrival.text = selectedText
                }
        
        //cityOrigion
          //  let NamesArry:[String] = ["Yes","No"]
        cityOrigion.delegate = self
        cityOrigion.optionArray = NamesArry
        cityOrigion.selectedRowColor = .white
        cityOrigion.listHeight = 300
        cityOrigion.rowHeight = 60
        cityOrigion.arrowColor  = UIColor.orange
          //  countryOrigion.arrowSize = 10
        cityOrigion.didSelect{(selectedText , index ,id) in

                self.cityOrigion.text = selectedText
                }
        
        //course
          //  let NamesArry:[String] = ["Yes","No"]
        course.delegate = self
        course.optionArray = NamesArry
        course.selectedRowColor = .white
        course.listHeight = 300
        course.rowHeight = 60
        course.arrowColor  = UIColor.orange
          //  countryOrigion.arrowSize = 10
        course.didSelect{(selectedText , index ,id) in
       self.course.text = selectedText
                }
        
        //gender
          //  let NamesArry:[String] = ["Yes","No"]
        gender.delegate = self
        gender.optionArray = NamesArry
        gender.selectedRowColor = .white
        gender.listHeight = 300
        gender.rowHeight = 60
        gender.arrowColor  = UIColor.orange
          //  countryOrigion.arrowSize = 10
        gender.didSelect{(selectedText , index ,id) in
       self.gender.text = selectedText
                }

        //intake
          //  let NamesArry:[String] = ["Yes","No"]
        intake.delegate = self
        intake.optionArray = NamesArry
        intake.selectedRowColor = .white
        intake.listHeight = 300
        intake.rowHeight = 60
        intake.arrowColor  = UIColor.orange
          //  countryOrigion.arrowSize = 10
        intake.didSelect{(selectedText , index ,id) in
       self.intake.text = selectedText
                }

        
        
           
        }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        countryOrigion.showList()
        countryDeparture.showList()
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
        
        
    }
    
    
    func validate()-> Bool {
            
            let thereAreEmptyTextFields = [countryOrigion, countryDeparture,cityDeparture,countryArrival,cityOrigion,course,gender,intake].map { $0.text ?? "" }.contains { $0.isEmpty }
            
            return thereAreEmptyTextFields
        }

    @IBAction func filterButtonAction(_ sender: Any) {
        
        let value = self.validate()
                
                if value == true { //
                   // if value == true
                    
                    debugPrint("Hi")
                    let alert = UIAlertController(title: "Alert", message: "please enter the details", preferredStyle: .alert)

                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

                    self.present(alert, animated: true)
                    
                }else{
                    print("sucess")
//        let htVC = (storyboard?.instantiateViewController(withIdentifier: "HomeTabBarViewController")) as? HomeTabBarViewController
//        htVC!.selectedIndex = 2
//
//
//        self.navigationController?.pushViewController(htVC!, animated: true)
                }
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  SortViewController.swift
//  NeoUniversity
//
//  Created by naveen kumar on 28/05/21.
//

import UIKit
import iOSDropDown
class SortViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var firtstName: DropDown!
    
    @IBOutlet weak var lastName: DropDown!
    @IBOutlet weak var arrivalDate: DropDown!
    @IBOutlet weak var arrivalTime: DropDown!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        //firstName
            let NamesArry:[String] = ["thulasi","ram"]
        firtstName.delegate = self
        firtstName.optionArray = NamesArry
        firtstName.selectedRowColor = .white
        firtstName.listHeight = 300
        firtstName.rowHeight = 60
        firtstName.arrowColor  = UIColor.orange
          //  countryOrigion.arrowSize = 10
        firtstName.didSelect{(selectedText , index ,id) in
            self.firtstName.text = selectedText
        }
        
        //last Name
           
        lastName.delegate = self
        lastName.optionArray = NamesArry
        lastName.selectedRowColor = .white
        lastName.listHeight = 300
        lastName.rowHeight = 60
        lastName.arrowColor  = UIColor.orange
          //  countryOrigion.arrowSize = 10
        lastName.didSelect{(selectedText , index ,id) in
            self.lastName.text = selectedText
        }
        
        
        //arrival Date
           
        arrivalDate.delegate = self
        arrivalDate.optionArray = NamesArry
        arrivalDate.selectedRowColor = .white
        arrivalDate.listHeight = 300
        arrivalDate.rowHeight = 60
        arrivalDate.arrowColor  = UIColor.orange
          //  countryOrigion.arrowSize = 10
        arrivalDate.didSelect{(selectedText , index ,id) in
            self.arrivalDate.text = selectedText
        }
        
        //arrival time
           
        arrivalTime.delegate = self
        arrivalTime.optionArray = NamesArry
        arrivalTime.selectedRowColor = .white
        arrivalTime.listHeight = 300
        arrivalTime.rowHeight = 60
        arrivalTime.arrowColor  = UIColor.orange
          //  countryOrigion.arrowSize = 10
        arrivalTime.didSelect{(selectedText , index ,id) in
            self.arrivalTime.text = selectedText
        }
    }
    
    @IBAction func applySortAction(_ sender: Any) {
        
       
       
    }
    
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
        
        
        
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


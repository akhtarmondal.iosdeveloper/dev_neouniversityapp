//
//  LaunchViewController.swift
//  NeoUniversity
//
//  Created by Kota Naveen Kumar on 16/07/21.
//

import UIKit

class LaunchViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.navigationBar.isHidden = true
        
        
        var token = ""
        if let  accesstoken =  NeoUniversityConstants.accessToken {
            token = accesstoken as! String
        }else{
            
        }
        
        if token != "" {
            let vv = (storyboard?.instantiateViewController(withIdentifier: "HomeTabBarViewController")) as! HomeTabBarViewController
            vv.selectedIndex = 2
            self.navigationController?.pushViewController(vv, animated: true)
            
        }else{
            
            let vv = (storyboard?.instantiateViewController(withIdentifier: "ViewController")) as! ViewController
            self.navigationController?.pushViewController(vv, animated: true)
            
        }

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

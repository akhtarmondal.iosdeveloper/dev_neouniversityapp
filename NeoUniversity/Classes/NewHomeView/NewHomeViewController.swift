//
//  NewHomeViewController.swift
//  NeoUniversity
//
//  Created by naveen kumar on 28/06/21.
//

import UIKit

class NewHomeViewController: UIViewController {
    @IBOutlet weak var universityupdatesbuttonAct: UIButton!
    
    let viewModel = HomeUniversityViewModel ()
    var response:GetUniversityStudentResponseModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.apicall()

        // Do any additional setup after loading the view.
    }
    
    
    func logoutMethod() {
        
        UserDefaults.standard.removeObject(forKey:"ACCESSTOKEN")
        UserDefaults.standard.synchronize()
        let vv = (storyboard?.instantiateViewController(withIdentifier: "ViewController")) as! ViewController
        self.navigationController?.pushViewController(vv, animated: true)
        
    }
    
    
    func apicall() {
        
        let myid:String = NeoUniversityConstants.universityId as! String
        let body = getUniversityStudentRequestModel.init(id:myid)
        viewModel.getUniversityStudentsApi(method: .GET, bodyparameters:body){
            [weak self] (resp) in
            
            guard self != nil else {return}
            
            self?.getStudentResponseModel(resp: resp)
            
        }
    }
    
    
    func getStudentResponseModel(resp:GetUniversityStudentResponseFinalModel) {
        
        if resp.status == true {
            
            self.response = resp.response
            let barViewControllers = self.tabBarController?.viewControllers
            let svc = barViewControllers![1] as! UniversityViewController
            svc.HomeData = resp.response
            
        }else{
            UtilityClass.displayAlertWith("API Error")
            self.logoutMethod()
        }
        
    }
    
    

    @IBAction func unvrstybtnAct(_ sender: Any) {
        
        
    
   let hvc = (storyboard?.instantiateViewController(withIdentifier: "HomeViewController")) as! HomeViewController
        hvc.response = response
        self.navigationController?.pushViewController(hvc, animated: true)

    }
   

}

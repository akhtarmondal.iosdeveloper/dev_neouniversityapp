//
//  OTPViewModel.swift
//  NeoStudent
//
//  Created by thulasi on 10/06/21.
//

import Foundation

class OTPViewModel {
    
    fileprivate let networkClient: NetworkClientProtocol
    
    init(networkClient: NetworkClientProtocol = NetworkClient.sharedInstance) {
        self.networkClient = networkClient
    }
    
}

extension OTPViewModel {
    
    func otpValidationApiCall(method:MethodType,bodyparameters:OtpValidateRequestModel ,onComplete: @escaping(finalOtpValidateResponseModel) -> ()){
        
        let url = NeoUniversityRequestUrl.OtpValide.url()
        
        let request = URLRequest.jsonRequest(url: url)
        self.networkClient.sendRequestGeneric(request: request, methodType: .post, bodyParams: bodyparameters){ (data, resp, err) in
            if let httpResponse = resp as? HTTPURLResponse {
                if httpResponse.statusCode != 200 && httpResponse.statusCode != 201 {
                    do {
                        let Response = try JSONDecoder().decode(ErrorModel.self, from: data!)
                        let final = finalOtpValidateResponseModel.init(sucessResponse:nil, errorResponse: Response)
                        onComplete(final)
                        
                    } catch let parsingError {
                        debugPrint("Error", parsingError)
                    }
                } else {
                    
                    do {
                        let Response = try JSONDecoder().decode(OtpValidateResponseModel.self, from: data!)
                        let final = finalOtpValidateResponseModel.init(sucessResponse:Response, errorResponse: nil)
                        onComplete(final)
                        
                    } catch let parsingError {
                        debugPrint("Error", parsingError)
                    }
                    
                }
                
                
            }
            
        }
    }
    
}

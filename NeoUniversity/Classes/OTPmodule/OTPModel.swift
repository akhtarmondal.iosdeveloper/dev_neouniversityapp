//
//  OTPModel.swift
//  NeoStudent
//
//  Created by Kota Naveen Kumar on 10/06/21.
//

import Foundation


// MARK: - ValidateRequestModel
struct OtpValidateRequestModel: Codable {
    let emailOtpType: EmailOtp?
    let mobileOtpType: MobileOtp?
    let type: String?
}








// MARK: - ValidateResponseModel
struct OtpValidateResponseModel: Codable {
    let accessToken, refreshToken: String?
}


// MARK: - ValidateResponseModel

struct finalOtpValidateResponseModel: Codable{
    let sucessResponse : OtpValidateResponseModel?
    let errorResponse : ErrorModel?
}

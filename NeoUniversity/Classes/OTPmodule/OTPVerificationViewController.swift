//
//  OTPVerificationViewController.swift
//  NeoUniversity
//
//  Created by naveen kumar on 25/05/21.
//

import UIKit
import Foundation
import SKActivityIndicatorView
import JWTDecode

let chatVC = ChatViewController()
let homeVC = HomeViewController()
let unversityVc = UniversityViewController()
let profileVC = ProfileViewController()
let settinsVC = SettingViewController()

class OTPVerificationViewController: UIViewController,UITextFieldDelegate {
    
    //Text Field fot otp
 
    var count = 10
    var resendTimer = Timer()
    var getMail = ""
    @IBOutlet weak var emaiId: UILabel!
    
    @IBOutlet weak var oneOtp: UITextField!
    
    @IBOutlet weak var twoOtp: UITextField!
    @IBOutlet weak var threeOtp: UITextField!
    @IBOutlet weak var fourOtp: UITextField!
    
//    var signupOTPtextFields: [OTPTextField]
//    {
//        return [oneOtp,twoOtp,threeOtp,fourOtp]
//    }
    @IBOutlet weak var resendButton: UIButton!
    
    @IBOutlet weak var timeLbl: UILabel!
    
    var signInInputModel: SignInResponseModel?
    var viewModel =  OTPViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showToast(message:"OTP Send Successfully --  \(signInInputModel?.emailOtp?.otp ?? 123)", font: UIFont().SFUITextFontStyle(style: .SFUITextRegular, size: 14), hight: 300)
        print ("getmail is  \(getMail)")
        resendButton.isHidden = true
        // Do any additional setup after loading the view
        self.navigationController?.navigationBar.isHidden = false
       // signupOTPtextFields.forEach { $0.backspaceTextFieldDelegate = self as BackspaceDelegate }
        emaiId.text = getMail
        
         oneOtp.delegate  = self
         twoOtp.delegate = self
         threeOtp.delegate = self
         fourOtp.delegate = self
        oneOtp.becomeFirstResponder()
        
        resendTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        
        let tabBarController = UITabBarController()

        tabBarController.viewControllers = [chatVC, unversityVc,homeVC,profileVC,settinsVC]
    }
    
    @objc func update()
    {
        if(count > 0) {
            count = count - 1
            
            timeLbl.text = "Wait \(count) seconds"
        }
        else {
            resendTimer.invalidate()
            timeLbl.isHidden = true
            resendButton.isHidden = false
        }
    }
    
    
        func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
            
            // Range.length == 1 means,clicking backspace
            if (textField.text!.count < 1) && (string.count > 0){
                   if textField == oneOtp {
                       twoOtp.becomeFirstResponder()
                   }
                   if textField == twoOtp {
                       threeOtp.becomeFirstResponder()
                   }
                   if textField == threeOtp {
                       fourOtp.becomeFirstResponder()
                   }
                   if textField == fourOtp {
                       fourOtp.becomeFirstResponder()
                   }
                 
                   textField.text = string
                   return false
               }
        else if (textField.text!.count >= 1) && (string.count == 0){
            if textField == twoOtp {
                oneOtp.becomeFirstResponder()
            }
            if textField == threeOtp {
                twoOtp.becomeFirstResponder()
            }
            if textField == fourOtp {
                threeOtp.becomeFirstResponder()
            }
            if textField == oneOtp {
                oneOtp.becomeFirstResponder()
            }
            textField.text = ""
            return false
        }
        else if (textField.text?.count)! >= 1{
            textField.text = string
            return false
        }
        return true

    }
    
    
    func validate()-> Bool {
            
            let thereAreEmptyTextFields = [oneOtp, twoOtp,threeOtp,fourOtp].map { $0.text ?? "" }.contains { $0.isEmpty }
            
            return thereAreEmptyTextFields
        }
    
    
    func validateOtpApiCall() {
            
            SKActivityIndicator.show()
            
            
            // if signInInputModel == nill call came from signUp page
            if signInInputModel != nil {
                
                let mobileModel = signInInputModel?.mobileOtp
                
                let emailmodel = signInInputModel?.emailOtp
                
                let body = OtpValidateRequestModel.init(emailOtpType: emailmodel, mobileOtpType: mobileModel, type: otpViewControllerConstants.email )
                
                viewModel.otpValidationApiCall(method: .POST, bodyparameters:body ) {
                    [weak self] (resp) in
                    
                    guard self != nil else {return}
                    
                    self?.validateOtpApiCalDisplay(resp:resp)
                    
                }
                
            }
            
           
        }

        
        func validateOtpApiCalDisplay(resp:finalOtpValidateResponseModel) {
            
            
            SKActivityIndicator.dismiss()
            
            
            if resp.sucessResponse?.accessToken == nil {
                        
                        UtilityClass.displayAlertWith("Wrong-- OTP")
                        
                    }else{
                        
                        let accessToken:String = (resp.sucessResponse?.accessToken!)!
                        UserDefaults.standard.setValue(accessToken, forKey:"ACCESSTOKEN")
                        let refreshToken:String = (resp.sucessResponse?.refreshToken!)!
                        UserDefaults.standard.setValue(refreshToken, forKey:"REFRESHTOKEN")
                        
                        
                  
                        let key = accessToken
                        do {
                            let jwt = try decode(jwt:key)
                            debugPrint(jwt.body)
                            let user = jwt.body["user"] as! [String : Any]
                            let university = user["university"] as! [String : Any]
                            let uniId:String = university["id"] as! String
                            debugPrint(uniId)
                            UserDefaults.standard.setValue(uniId, forKey:"UNIVERSITYID")
                        } catch {
                          print("Failed to decode JWT: \(error)")
                        }
                        
                        UserDefaults.standard.synchronize()
                        let htVC = (storyboard?.instantiateViewController(withIdentifier: "HomeTabBarViewController")) as? HomeTabBarViewController
                        htVC!.selectedIndex = 2

                        
                        self.navigationController?.pushViewController(htVC!, animated: true)
                    }
            
            debugPrint(resp)
            
            
            }
    
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
        let value = self.validate()
                
                if value == true { //
                   // if value == true
                    
                    debugPrint("Hi")
                    let alert = UIAlertController(title: "Alert", message: "please enter the OTP", preferredStyle: .alert)

                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

                    self.present(alert, animated: true)
                    
                }else{
                    
                    self.validateOtpApiCall()
       
                }
        
    }
    
    
    //MARK:-  Back  Button Action
//
//     @IBAction func buttonAction(_ sender: Any) {
//         if oneOtp.text == "" {
//             oneOtp.becomeFirstResponder()
//         } else if twoOtp.text == "" {
//             twoOtp.becomeFirstResponder()
//         } else if threeOtp.text == "" {
//             threeOtp.becomeFirstResponder()
//         } else if fourOtp.text == "" {
//             fourOtp.becomeFirstResponder()
//         }
//     }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}

///// for deleting the otp we are using backspace  function for otp style text field
//extension OTPVerificationViewController: BackspaceDelegate {
//    func textFieldDidEnterBackspace(_ textField: OTPTextField) {
//        guard let index = signupOTPtextFields.firstIndex(of: textField) else {
//            return
//        }
//
//        if index > 0 {
//            signupOTPtextFields[index - 1].becomeFirstResponder()
//        } else {
//            signupOTPtextFields[0].becomeFirstResponder()
//        }
//    }
//}

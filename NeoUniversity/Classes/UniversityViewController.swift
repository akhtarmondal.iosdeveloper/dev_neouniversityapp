//
//  UniversityViewController.swift
//  NeoUniversity
//
//  Created by naveen kumar on 03/06/21.
//

import UIKit

class UniversityViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var uniNameLabel: UILabel!
    
    
    var data = ["Thulasiram","Thulasiram", "Thulasiram","Thulasiram",
    "Thulasiram"]
    
    var HomeData:GetUniversityStudentResponseModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.uniNameLabel.text = HomeData?.name
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        cell.textLabel?.text = data[indexPath.row]
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
}


//
//  OTPTextField.swift
//  CUSTOMERNEW
//
//  Created by thulasi on 20/03/19.
//  Copyright © 2019 NeumTech. All rights reserved.
//

import UIKit


protocol BackspaceDelegate: class {
    func textFieldDidEnterBackspace(_ textField: OTPTextField)
}
class OTPTextField: UITextField {
    weak var backspaceTextFieldDelegate: BackspaceDelegate?
    
    override func deleteBackward() {
        if text?.isEmpty ?? false {
            backspaceTextFieldDelegate?.textFieldDidEnterBackspace(self)
        }
        
        super.deleteBackward()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // custom setup
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // custom setup
    }

}



//
//  UtilityClass.swift
//  NeoStudent
//
//  Created by Kota Naveen Kumar on 10/06/21.
//

import UIKit
import Foundation

class UtilityClass: NSObject {

}
extension UtilityClass {
    
    class func displayAlertWith(_ message: String) {
        
        
        let alert = UIAlertController(title:"Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:"Ok", style: .default, handler: nil))
        DispatchQueue.main.async(execute: {
            UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        })
    }
    
    // Convert from NSData to json object
    class  func dataToJSON(_ data: Data) -> AnyObject? {
     do {
            let  JSON =  try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            debugPrint("VALID JSON \(JSON) VALID JSON ")
            
        } catch let myJSONError {
            debugPrint(myJSONError)
        }
        return nil
    }
}


//    This extension is used to get top most ViewController in order to show all type of alerts correctly
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)

        }
        
        return controller
    }
    

        
    //    This extension is used to get top most ViewController in order to show all type of alerts correctly
    /*
        class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
            
            if let navigationController = controller as? UINavigationController {
                return topViewController(controller: navigationController.visibleViewController)
            }
            
            if let tabController = controller as? UITabBarController {
                if let selected = tabController.selectedViewController {
                    return topViewController(controller: selected)
                }
            }
            
            if let presented = controller?.presentedViewController {
                return topViewController(controller: presented)

            }
            
            return controller
            
        }
        */
    

}

extension URLRequest {
    static func jsonRequest(url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("authorization", forHTTPHeaderField: "CIGfMA0GCSqGSIb3DQEBAQdqDup1pgSc0tQUMQUAA4GNADCBiQKBgQD3apAg6H2i")
        return request
    }
}

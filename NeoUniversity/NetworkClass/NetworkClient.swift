//
//  NetworkClient.swift
//  NeoStudent
//
//  Created by Kota Naveen Kumar on 10/06/21.
//

import Foundation
import UIKit
import MobileCoreServices
import Alamofire

// MARK: - NetworkError
/// - generic:    Generic error
/// - invalidURL: Invalid URL error
enum NetworkError: String, Error {
    case generic
    case invalidURL
    case ServerProblem = "Could not connect to the server."
}

enum MethodType: String {
    case POST = "POST"
    case GET = "GET"
    case MULTIPARTWITH_POST = "MULTIPARTWITH_POST"
    
}
// MARK: - NetworkClientProtocol
protocol NetworkClientProtocol {
    
    
    func sendRequestGeneric<T: Encodable>(request: URLRequest,methodType: HTTPMethod,bodyParams:T, completion: @escaping (Data?, URLResponse?, Error?) -> ())
    
}

// MARK: - NetworkClient
class  NetworkClient: NetworkClientProtocol {
    
    static let sharedInstance = NetworkClient()
    
    func sendRequestGeneric<T: Encodable>(request: URLRequest, methodType: HTTPMethod, bodyParams: T, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        
        
        if NetworkReachability.isConnectedToNetwork() == false {
            UtilityClass.displayAlertWith("CHECKURINTERNETCONNECTION")
            completion(nil, nil, NetworkError.invalidURL)
            return
        }
        guard let url = request.url else {
            completion(nil, nil, NetworkError.invalidURL)
            return
        }
        
        let token = "".AppendingToken()
        var headers:[String:String] = [:]
        if token != "" {
            headers = ["Content-Type":"application/json","Authorization":"\(token)"] // "Content-Type":"application/json"
        }else{
            headers = ["Content-Type":"application/json"]
        }
        
        
        if methodType == .get {
            
            Alamofire.request(url, method: methodType, headers:headers)
                .responseJSON { response in
                    if let httpStatus = response.response, httpStatus.statusCode == 200 {
                        do{
                            completion(response.data,response.response,response.error)
                        }
                    }else{
                        completion(response.data,response.response,response.error)
                    }
            }
            
            
        }else{
            
            let jsonData = try! JSONEncoder().encode(bodyParams)
            let json = try! JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
            
           
            Alamofire.request(url, method: methodType, parameters: (json as! Parameters), encoding: JSONEncoding.default, headers:headers)
                .responseJSON { response in
                    if let httpStatus = response.response, httpStatus.statusCode == 200 {
                        do{
                            completion(response.data,response.response,response.error)
                        }
                    }else{
                        completion(response.data,response.response,response.error)
                    }
            }
        }
            
            
        }
    
}
extension Data {
    mutating func append(string: String) {
        let data = string.data(
            using: String.Encoding.utf8,
            allowLossyConversion: true)
        append(data!)
    }
} 



extension String  {
    
    func AppendingToken() -> String {
        
        let value = NeoUniversityConstants.accessToken
        
        if value != nil {
            return "Bearer \(value ?? "")"
        }else{
            
            var value1:String = ""
            
            if  let value =  UserDefaults.standard.value(forKey:"ACCESSTOKEN") {
                
                value1 = value as! String
                
            }else {
                
            }
            
            if value1 != "" {
                return "Bearer \(value1)"
            }else{
                return ""
            }
        }
        
        
    }
}
